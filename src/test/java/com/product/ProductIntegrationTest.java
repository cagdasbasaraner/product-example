package com.product;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.product.model.payload.ProductPayload;
import com.product.model.response.ProductResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

/**
 * Integration test which tests controller->service->repository flow together, without mocking.
 */
@SpringBootTest
@AutoConfigureMockMvc
public class ProductIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testAddGetDeleteProduct() throws Exception {

        ProductPayload payload = new ProductPayload();
        payload.setName(UUID.randomUUID().toString());
        payload.setPrice(1.0f);
        payload.setStock(1);

        String contentAsString = mvc.perform(MockMvcRequestBuilders
                        .post("/products")
                        .content(new ObjectMapper().writeValueAsString(payload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

        ProductResponse addedProduct = new ObjectMapper().readValue(contentAsString, ProductResponse.class);

        Assertions.assertNotNull(addedProduct.getId());

        String productId = addedProduct.getId();
        contentAsString = mvc.perform(MockMvcRequestBuilders
                        .get("/products/" + productId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

        ProductResponse returnedProduct = new ObjectMapper().readValue(contentAsString, ProductResponse.class);

        Assertions.assertEquals(addedProduct, returnedProduct);

        mvc.perform(MockMvcRequestBuilders
                        .delete("/products/" + productId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        mvc.perform(MockMvcRequestBuilders
                        .get("/products/" + productId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
