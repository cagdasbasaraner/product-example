package com.product.client;

import com.product.exception.ApiClientException;
import com.product.exception.ProductNotFoundException;
import com.product.model.ActivityProduct;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ApiClientTest {

    @Autowired
    ApiClientWrapper apiClientWrapper;

    @Test
    public void testActivityProducts() throws ProductNotFoundException, InterruptedException {
        ActivityProduct activityProduct = this.apiClientWrapper.getActivityProduct();

        Assertions.assertNotNull(activityProduct.getActivity());
        Assertions.assertNotNull(activityProduct.getAccessibility());
        Assertions.assertNotNull(activityProduct.getKey());
        Assertions.assertNotNull(activityProduct.getType());
        Assertions.assertNotNull(activityProduct.getParticipants());
    }
}
