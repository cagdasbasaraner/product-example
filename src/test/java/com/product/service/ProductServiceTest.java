package com.product.service;

import com.product.client.ApiClientWrapper;
import com.product.exception.ProductNotFoundException;
import com.product.model.Product;
import com.product.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class ProductServiceTest {

    private ProductRepository productRepository;
    private ProductService productService;
    private ApiClientWrapper apiClientWrapper;

    @BeforeEach
    public void setup(){
        productRepository = mock(ProductRepository.class);
        apiClientWrapper = mock(ApiClientWrapper.class);
        productService = new ProductServiceImpl(productRepository, apiClientWrapper);
    }

    @Test
    public void testGet() throws ProductNotFoundException {
        String dummyProductId = UUID.randomUUID().toString();
        when(productRepository.get(dummyProductId)).thenReturn(Optional.of(new Product()));
        productService.getProduct(dummyProductId);
        verify(productRepository).get(dummyProductId);
    }

    @Test
    public void testList() {
        productService.listAllProducts();
        verify(productRepository).list();
    }

    @Test
    public void testDelete() throws ProductNotFoundException {
        String dummyProductId = UUID.randomUUID().toString();
        Product dummyProduct = new Product();
        dummyProduct.setId(dummyProductId);
        when(productRepository.get(dummyProductId)).thenReturn(Optional.of(dummyProduct));
        productService.deleteProduct(dummyProductId);
        verify(productRepository).get(dummyProductId);
        verify(productRepository).remove(dummyProductId);
    }

    @Test
    public void testAdd() {
        String dummyProductId = UUID.randomUUID().toString();
        Product dummyProduct = new Product();
        dummyProduct.setId(dummyProductId);
        productService.addProduct(dummyProduct);
        verify(productRepository).add(dummyProduct);
    }

    @Test
    public void testUpdate() throws ProductNotFoundException {
        String dummyProductId = UUID.randomUUID().toString();
        Product dummyProduct = new Product();
        dummyProduct.setId(dummyProductId);
        when(productRepository.get(dummyProductId)).thenReturn(Optional.of(dummyProduct));
        productService.updateProduct(dummyProduct);
        verify(productRepository).get(dummyProductId);
        verify(productRepository).update(dummyProduct);
    }

    @Test
    public void testPatch() throws ProductNotFoundException {
        String dummyProductId = UUID.randomUUID().toString();
        Integer stock = 10;
        Product dummyProduct = new Product();
        dummyProduct.setId(dummyProductId);
        when(productRepository.get(dummyProductId)).thenReturn(Optional.of(dummyProduct));
        productService.updateProductStock(dummyProductId, stock);
        verify(productRepository).get(dummyProductId);
        verify(productRepository).update(dummyProduct);
    }
}
