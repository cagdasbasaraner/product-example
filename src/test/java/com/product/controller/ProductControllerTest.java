package com.product.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.product.exception.ProductNotFoundException;
import com.product.mapper.ProductDtoMapper;
import com.product.model.Product;
import com.product.model.payload.ProductPayload;
import com.product.model.payload.ProductStockPayload;
import com.product.model.response.ProductResponse;
import com.product.repository.ProductRepository;
import com.product.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService productService;
    @MockBean
    private ProductDtoMapper productDtoMapper;
    @MockBean
    private ProductRepository productRepository;

    @Test
    public void testNotFound() throws Exception, ProductNotFoundException {
        String nonExistentProductId = "123";
        when(productService.getProduct(nonExistentProductId)).thenThrow(new ProductNotFoundException(anyString()));
        mvc.perform(MockMvcRequestBuilders
                        .get("/products/" + nonExistentProductId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testAdd() throws Exception {
        ProductPayload payload = new ProductPayload();
        payload.setName(UUID.randomUUID().toString());
        payload.setPrice(1.0f);
        payload.setStock(1);

        mvc.perform(MockMvcRequestBuilders
                        .post("/products")
                        .content(new ObjectMapper().writeValueAsString(payload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testBadRequest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post("/products")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testGet() throws Exception, ProductNotFoundException {
        String nonExistentProductId = "123";
        Product dummyProduct = new Product();
        when(productService.getProduct(nonExistentProductId)).thenReturn(dummyProduct);
        when(productDtoMapper.constructResponseFromProduct(dummyProduct)).thenReturn(new ProductResponse());
        mvc.perform(MockMvcRequestBuilders
                        .get("/products/" + nonExistentProductId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
        verify(productService).getProduct(nonExistentProductId);
        verify(productDtoMapper).constructResponseFromProduct(dummyProduct);
    }

    @Test
    public void testList() throws Exception {
        when(productService.listAllProducts()).thenReturn(Collections.emptyList());
        mvc.perform(MockMvcRequestBuilders
                        .get("/products")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
        verify(productService).listAllProducts();
    }

    @Test
    public void testDelete() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .delete("/products/123")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testUpdate() throws Exception, ProductNotFoundException {

        String dummyProductId = "123";

        ProductPayload payload = new ProductPayload();
        payload.setName(UUID.randomUUID().toString());
        payload.setPrice(1.0f);
        payload.setStock(1);

        Product dummyProduct = new Product();
        dummyProduct.setPrice(payload.getPrice());
        dummyProduct.setStock(payload.getStock());
        dummyProduct.setName(payload.getName());
        dummyProduct.setId(dummyProductId);

        when(productDtoMapper.constructProductFromPayload(Optional.of(dummyProductId), payload)).thenReturn(dummyProduct);

        mvc.perform(MockMvcRequestBuilders
                        .put("/products/" + dummyProductId)
                        .content(new ObjectMapper().writeValueAsString(payload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(productService).updateProduct(dummyProduct);
        verify(productDtoMapper).constructProductFromPayload(Optional.of(dummyProductId), payload);
    }

    @Test
    public void testPatch() throws Exception, ProductNotFoundException {

        String dummyProductId = "123";

        ProductStockPayload payload = new ProductStockPayload();
        payload.setStock(10);

        mvc.perform(MockMvcRequestBuilders
                        .patch("/products/" + dummyProductId + "/stock")
                        .content(new ObjectMapper().writeValueAsString(payload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(productService).updateProductStock(dummyProductId, payload.getStock());
    }
}
