package com.product.mapper;

import com.product.model.Product;
import com.product.model.payload.ProductPayload;
import com.product.model.response.ProductListResponse;
import com.product.model.response.ProductResponse;

import java.util.List;
import java.util.Optional;

public interface ProductDtoMapper {

    Product constructProductFromPayload(Optional<String> productId, ProductPayload productPayload);

    ProductResponse constructResponseFromProduct(Product product);

    ProductListResponse constructResponseFromProductList(List<Product> product);
}
