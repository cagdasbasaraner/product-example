package com.product.service;

import com.product.client.ApiClientWrapper;
import com.product.exception.ApiClientException;
import com.product.exception.ProductNotFoundException;
import com.product.model.ActivityProduct;
import com.product.model.Product;
import com.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private ApiClientWrapper apiClientWrapper;


    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, ApiClientWrapper apiClientWrapper) {
        this.productRepository = productRepository;
        this.apiClientWrapper = apiClientWrapper;
    }

    @Override
    public void deleteProduct(String productId) throws ProductNotFoundException {
        Product product = checkProductExistence(productId);
        productRepository.remove(product.getId());
    }

    @Override
    public Product getProduct(String productId) throws ProductNotFoundException {
        return checkProductExistence(productId);
    }

    @Override
    public Product addProduct(Product product) {
        return productRepository.add(product);
    }

    @Override
    public void updateProduct(Product product) throws ProductNotFoundException {
        checkProductExistence(product.getId());
        productRepository.update(product);
    }

    @Override
    public void updateProductStock(String productId, int stock) throws ProductNotFoundException {
        Product productToBeUpdated = checkProductExistence(productId);
        productToBeUpdated.setStock(stock);
        productRepository.update(productToBeUpdated);
    }

    @Override
    public List<Product> listAllProducts() {
        return productRepository.list();
    }

    @Override
    public Product checkProductExistence(String productId) throws ProductNotFoundException {
        Optional<Product> product = productRepository.get(productId);
        if (!product.isPresent()) {
            throw new ProductNotFoundException("product does not exist!");
        }
        return product.get();
    }

    @Override
    public ActivityProduct getActivityProduct() throws ProductNotFoundException, InterruptedException {
        return apiClientWrapper.getActivityProduct();
    }
}