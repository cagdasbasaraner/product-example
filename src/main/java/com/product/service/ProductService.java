package com.product.service;

import com.product.exception.ApiClientException;
import com.product.exception.ProductNotFoundException;
import com.product.model.ActivityProduct;
import com.product.model.Product;
import com.product.model.payload.ProductPayload;

import java.io.IOException;
import java.util.List;

/**
 * Interface for service layer of product operations.
 *
 * TODO : for the next version request objects (Add/Get/Remove/UpdateProductRequest) can be used instead of entity object.
 */
public interface ProductService {

    Product getProduct(String productId) throws ProductNotFoundException;

    Product addProduct(Product product);

    void updateProduct(Product product) throws ProductNotFoundException;

    void updateProductStock(String productId, int stock) throws ProductNotFoundException;

    void deleteProduct(String productId) throws ProductNotFoundException;

    List<Product> listAllProducts();

    Product checkProductExistence(String productId) throws ProductNotFoundException;

    ActivityProduct getActivityProduct() throws InterruptedException, ProductNotFoundException;
}
