package com.product.repository;

import com.product.model.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class ProductRepositoryImpl implements ProductRepository {

    /**
     * dummy in-memory key-value database for products
     */
    private Map<String, Product> productDatabase = new ConcurrentHashMap<>();

    @Override
    public Product add(Product product) {
        product.setId(UUID.randomUUID().toString());
        productDatabase.put(product.getId(), product);
        return product.clone();
    }

    @Override
    public void remove(String productId) {
        productDatabase.remove(productId);
    }

    @Override
    public void update(Product product) {
        productDatabase.replace(product.getId(), product);
    }

    @Override
    public Optional<Product> get(String productId) {
        Product product = productDatabase.get(productId);
        return Optional.ofNullable(product);
    }

    @Override
    public List<Product> list() {
        return productDatabase.values().stream().collect(Collectors.toList());
    }
}
