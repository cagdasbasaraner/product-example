package com.product.repository;

import com.product.model.Product;

import java.util.List;
import java.util.Optional;

/**
 * Dummy in-memory repository for products.
 * Since this is an example project, a real database is not used to ease running the project.
 */
public interface ProductRepository {

    Product add(Product product);

    void remove(String productId);

    void update(Product product);

    Optional<Product> get(String productId);

    /**
     * This method might have pagination parameter on the next version.
     * @return <code>List<Product></code>
     */
    List<Product> list();
}
