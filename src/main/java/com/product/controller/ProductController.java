package com.product.controller;

import com.product.exception.ApiClientException;
import com.product.exception.ProductNotFoundException;
import com.product.mapper.ProductDtoMapper;
import com.product.model.ActivityProduct;
import com.product.model.Product;
import com.product.model.payload.ProductPayload;
import com.product.model.payload.ProductStockPayload;
import com.product.model.response.ProductListResponse;
import com.product.model.response.ProductResponse;
import com.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductDtoMapper productDtoMapper;

    @GetMapping(value = "/{productId}")
    public ResponseEntity getProduct(@PathVariable("productId") String productId) throws ProductNotFoundException {
        Product product = productService.getProduct(productId);
        ProductResponse productResponse = productDtoMapper.constructResponseFromProduct(product);
        return ResponseEntity.ok(productResponse);
    }

    @PostMapping()
    public ResponseEntity addProduct(@Valid @RequestBody ProductPayload productPayload) {
        Product product = productDtoMapper.constructProductFromPayload(Optional.empty(), productPayload);
        return ResponseEntity.ok(productService.addProduct(product));
    }

    @PutMapping(value = "/{productId}")
    public ResponseEntity updateProduct(@Valid @RequestBody ProductPayload productPayload,
                                        @PathVariable(value = "productId") String productId) throws ProductNotFoundException {
        Product product = productDtoMapper.constructProductFromPayload(Optional.of(productId), productPayload);
        productService.updateProduct(product);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(value = "/{productId}/stock")
    public ResponseEntity updateProductStock(@Valid @RequestBody ProductStockPayload productStockPayload,
                                             @PathVariable(value = "productId") String productId) throws ProductNotFoundException {
        productService.updateProductStock(productId, productStockPayload.getStock());
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/{productId}")
    public ResponseEntity deleteProduct(@PathVariable(value = "productId") String productId) throws ProductNotFoundException {
        productService.deleteProduct(productId);
        return ResponseEntity.ok().build();
    }

    @GetMapping()
    public ResponseEntity listAllProducts() {
        ProductListResponse productListResponse = productDtoMapper.constructResponseFromProductList(productService.listAllProducts());
        return ResponseEntity.ok(productListResponse);
    }

    @GetMapping(value = "/activity")
    public ActivityProduct getActivityProduct() throws InterruptedException, ProductNotFoundException {
        return productService.getActivityProduct();
    }
}
