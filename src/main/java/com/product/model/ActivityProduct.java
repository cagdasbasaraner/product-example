package com.product.model;

import lombok.Data;

@Data
public class ActivityProduct {

    private String activity;
    private String type;
    private String link;
    private String key;
    private String accessibility;
    private Integer participants;
    private Float price;
}
