package com.product.model;

import lombok.Data;

@Data
public class Product {

    private String id;
    private String name;
    private int stock;
    private float price;

    public Product clone() {
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setStock(stock);
        product.setPrice(price);
        return product;
    }
}
