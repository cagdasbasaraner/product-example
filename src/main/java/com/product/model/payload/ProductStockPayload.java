package com.product.model.payload;

import lombok.Data;

import javax.validation.constraints.Min;

@Data
public class ProductStockPayload {

    @Min(value = 1, message ="stock should be a positive number")
    private int stock;
}
