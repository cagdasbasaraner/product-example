package com.product.model.payload;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ProductPayload {

    @NotNull (message =  "name should be defined")
    private String name;

    private int stock = 0;

    private float price;
}
