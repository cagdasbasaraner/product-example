package com.product.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProductListResponse {

    @JsonProperty("products")
    private List<ProductResponse> products = new ArrayList<>();
}
