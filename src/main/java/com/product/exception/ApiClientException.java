package com.product.exception;

public class ApiClientException extends Throwable {
    public ApiClientException(String message) {
        super(message);
    }
}
