package com.product.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name="sample-api-call", url = "https://www.boredapi.com/api")
public interface ApiClient {

	@GetMapping("/activity")
	String getActivityProduct();
}