package com.product.client;

import com.product.exception.ProductNotFoundException;
import com.product.model.ActivityProduct;

public interface ApiClientWrapper {

    ActivityProduct getActivityProduct() throws InterruptedException, ProductNotFoundException;
}
