package com.product.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.product.exception.ApiClientException;
import com.product.exception.ProductNotFoundException;
import com.product.model.ActivityProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class ApiClientWrapperImpl implements ApiClientWrapper {

    @Autowired
    private ApiClient apiClient;

    private static final int WAIT_DURATION = 100;
    private static final int MAX_RETRIES = 3;

    @Override
    public ActivityProduct getActivityProduct() throws InterruptedException, ProductNotFoundException {
        for (int retryCounter = 0; retryCounter <= MAX_RETRIES; retryCounter++) {
            try {
                String response = apiClient.getActivityProduct();
                return new ObjectMapper().readValue(response, ActivityProduct.class);
            } catch (Exception ex) {
                // TODO : log error
                // throw exception if the last re-try fails
                TimeUnit.MILLISECONDS.sleep(WAIT_DURATION * retryCounter);
                if (retryCounter == MAX_RETRIES) {
                    throw new ProductNotFoundException("No activity product exist at the moment.");
                }
            }
        }
        throw new ProductNotFoundException("No activity product exist at the moment.");
    }
}
