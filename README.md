### Design Decisions

- This is a simple product api, which enables to create, update, delete, get and list products.
- Java 17 and Spring Boot is used to implement the api.
- As an example to 'external api call', I added another endpoint '/products/activity' which calls an external public api and gets an activity object from there and returns it as an activity product.
- For external API call, FeignClient approach is used. Also, a very simple implementation of retry logic added.
- The structure is layered, there are Controller, Service, Repository layers. API design conforms to bast practices.
- Repository layer is a dummy in-memory database implemented with a Map here, for simplicity.
- Lombok is used to prevent adding getter/setter and equals/hsCode methods for simplicity.
- There are specific exceptions for error cases and handled by ExceptionHandlerAdvice structure to return correct error codes, which is a cleaner and maintainable approach.
- To make layers more independent of each other, DtoMapper is used to convert payloads and entities to a different format.
- There are high coverage unit tests for Controller, Service, ApiClient layers and also an integration test exists.
- 

### Possible Improvements
- In a real system, a real DB and repository layer should be used.
- For external api calls, timeout support should be added to limit max call duration. Also, retry-logic code can be wrapped into a class and used as a util for multiple calls. Another approach can be used instead of Thread.sleep.
- If there would be a real DB layer, unit tests should exist for it. Simple unit tests can also be added for DtoMapper. Test cases can be increased for Service, Controller, ApiClient tests.
- For list endpoint, there should be pagination support for a real life scenario.
- If there will be very high traffic, async approaches (like using a queue and queue processor to handle requests) can be used.
- 